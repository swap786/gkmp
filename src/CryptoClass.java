import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptoClass
{
	static String plaintext = "test text 123akjhf;owjhfpoweqhfp2y3h-0fhpubpwqiugfpwe";// 
	
	/*Example using AES  Lovely dated 12 October 2014
	 * Default block size for AES is 128 bit,16 bytes

This example uses AES-128 to encrypt a short message "Hello" using the 128-bit key 0xA456B7A422C5145ABCF2B3CB206579A8. The block size of 128 bits is equal to 128/8 = 16 bytes, so we need to pad our 5-byte plaintext with an 11-byte padding string. The ASCII characters "Hello" are 48656C6C6F in hexadecimal and 11 decimal = 0x0B.
Method 1 PKCS#7/RFC3369 method
AES INPUT BLOCK  = 48656C6C6F0B0B0B0B0B0B0B0B0B0B0B
AES OUTPUT BLOCK = 42FF63CC06D53DA93F24389723E1611A

Method 2 Pad with 0x80 + nulls
AES INPUT BLOCK  = 48656C6C6F8000000000000000000000
AES OUTPUT BLOCK = 97AFA1455DA9E2E1B821275997CF4DC5

Method 3 Pad with nulls + # bytes
AES INPUT BLOCK  = 48656C6C6F000000000000000000000B
AES OUTPUT BLOCK = 6024D07C11283639425E3A33D99F32BA

Method 4 Pad with nulls
AES INPUT BLOCK  = 48656C6C6F0000000000000000000000 
AES OUTPUT BLOCK = 3FC6C30A64CD6E2970803871C7068998

Method 5 Pad with spaces
AES INPUT BLOCK  = 48656C6C6F2020202020202020202020
AES OUTPUT BLOCK = 7FB72E7BE929223D001E3129DFFA20A0
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	static String encryptionKey = "0123456789abcdef";// 64bit Key and Block Size is of 128 bit
/*You can use the AES Calculator  to encrypt or decrypt using AES
 *  the specified 128-bit (32 hex digit) data value with the 128/192/256-bit
 *   (32/48/64 hex digit) key, with a trace of the calculations.
 *    Some example values which may be used are given below.
 *    
 *    examples of 32hexKey:    
 *        000102030405060708090a0b0c0d0e0f
Plaintext:  00112233445566778899aabbccddeeff
Ciphertext: 69c4e0d86a7b0430d8cdb78070b4c55a

12 October 2014
 */
	public static void main(String[] args)
	{
		try
		{
			byte[] cipher = encrypt(encryptionKey, plaintext.getBytes());

			System.out.print("cipher:  ");
			for (int i = 0; i < cipher.length; i++)
				System.out.print(new Integer(cipher[i]) + " ");
			System.out.println("");

			byte[] decrypted = decrypt(encryptionKey, cipher);

			System.out.println("decrypt: " + new String(decrypted));

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static byte[] encrypt(String key, byte[] value)
	{
		try
		{
			byte[] raw = key.getBytes();
			if (raw.length != 16)
			{
				throw new IllegalArgumentException("Invalid key size.");
			}

			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(
					new byte[16]));
			return cipher.doFinal(value);
		}
		catch (Exception ex)
		{
			return value;
		}
	}

	public static byte[] decrypt(String key, byte[] encrypted)
	{
		try
		{
			byte[] raw = key.getBytes();
			if (raw.length != 16)
			{
				throw new IllegalArgumentException("Invalid key size.");
			}
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(
					new byte[16]));
			byte[] original = cipher.doFinal(encrypted);

			return original;
		}
		catch (Exception ex)
		{
			return encrypted;
		}
	}
}
