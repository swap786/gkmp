// This GKP class is used for the purpose of generating the current key and 
// Next key, Yaha par apan bohot si method use ki hai jaise getCurrentKey(),
// It is use for returning Currentkey value when called.
// setCurrentKey() : Yaha apan check karenge ki ye key 16 bit se badi hai ya choti
// agar choti hai to apan "" blank space add karenge.
// agar 16 bit se badi hai to we will use the function substring(0,16) to only 
// have 16 bit key.
// Same procedure is repeated for getNextkey() for processing NextKey.
// 1 aur method yaha use kiye toString jo dono value return karenga.

public class GKP
{
	private String CurrentKey;
	private String NextKey;

	public String getCurrentKey()
	{
		return CurrentKey;
	}

	public void setCurrentKey(String currentKey)// Here we are setting the current key
	{                                    // We will check whether the currentkey is equal to 16 or not
		if (currentKey.length() < 16)
		{
			for (int count = currentKey.length(); count <= 16; count++)
				currentKey = currentKey + " ";
		}
		else if (currentKey.length() > 16)
			currentKey = currentKey.substring(0, 16);// Agar key 16 se jyada hai to then apan substring
                                // use karke starting ke 16 bit use karke key generate karenge
		CurrentKey = currentKey;
	}

	public String getNextKey()
	{
		return NextKey;
	}

	public void setNextKey(String nextKey)
	{
		if (nextKey.length() < 16)
		{
			for (int count = nextKey.length(); count <= 16; count++)
				nextKey = nextKey + " ";
		}
		else if (nextKey.length() > 16)
			nextKey = nextKey.substring(0, 16);

		NextKey = nextKey;
	}

	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return CurrentKey + "," + NextKey;
	}
}
