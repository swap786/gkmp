/*
Class GroupHead Line 240 onwards we r using encryption by means of GKP and Crypto Class


*/
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.*;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.omg.CORBA.CurrentOperations;
import java.io.*;
import java.net.*;
import java.util.*;
public class GroupHead implements CaretListener
{
	public Thread thrJoinThread;
	public Thread thrCommunicateThread;
	public ServerSocket listener = null;
	public ArrayList<Socket> lstSockets;
	public ArrayList<Socket> lstfSockets;
	public ArrayList<GKP> lstGroupKeyPackets;
	public int CURRENT_OPERATION = 0;
	public BufferedReader inputReader;
    String path="";
	JFrame frame;
	JButton btnStart;
	JButton btnStop;
	JButton btnBroadcast,btnAttachFile;
	JButton btnRemoveMember;
	JTextField txtPortNumber, txtFDetails;
	JTextField txtMessage;

	public static void main(String[] args) throws IOException
	{
		new GroupHead();
	}

	public GroupHead()
	{
		try
		{
			frame = new JFrame("Welcome to GKMP: Group Head");
			frame.setSize(400, 400);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			frame.setLayout(new GridLayout(9, 1));

			btnStart = new JButton("Start Group Head Thread");
			btnStart.addActionListener(new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					lstSockets = new ArrayList<Socket>();
					lstfSockets = new ArrayList<Socket>();
					lstGroupKeyPackets = new ArrayList<GKP>();
					inputReader = new BufferedReader(new InputStreamReader(
							System.in));

					// //////////////////////////////////////////////
					// Start the Join Thread
					// //////////////////////////////////////////////
					thrJoinThread = new Thread(new Runnable()
					{
						@Override
						public void run()
						{
							try
							{
								int PORT_NUM = Integer.parseInt(txtPortNumber
										.getText());
								listener = new ServerSocket(PORT_NUM);

								// Generate a secret number so that the
								// participants in the
								// group can join
								int SECRET_NUMBER = (int) Math.round(Math
										.random() * 50);
								System.out
										.println("This group's secret key is "
												+ SECRET_NUMBER);

								thrCommunicateThread.start();
								while (true)
								{
									Socket socket = listener.accept();

									// Open the read stream and get the secret
									// key number
									BufferedReader sreader = new BufferedReader(
											new InputStreamReader(socket
													.getInputStream()));
									int AUTH_KEY = sreader.read();
									System.out.println("AUTH_KEY: "+AUTH_KEY);
									if (AUTH_KEY == SECRET_NUMBER)
									{
										// Generate group keys
										GKP gkp = new GKP();
										gkp.setCurrentKey(KeyGenerator
												.generateKey(16));
										gkp.setNextKey(KeyGenerator
												.generateKey(16));
										lstGroupKeyPackets.add(gkp);

										System.out.println("Keys are " + gkp);

										lstSockets.add(socket);
									txtFDetails.setText("\n"+txtFDetails.getText()+"\nClient with IP: "+socket.getRemoteSocketAddress().toString()+" Connected.");
										//lstfSockets.add(socket);
										System.out
												.println("Number of users in system:"
														+ lstSockets.size());
									}
									else if(AUTH_KEY == SECRET_NUMBER+1)
									{
										lstfSockets.add(socket);
									}
									else
									{
										System.out
												.println("Invalid request from "
														+ socket.toString()
														+ ",Expected:"
														+ SECRET_NUMBER
														+ ",Received:"
														+ AUTH_KEY);
										socket.getOutputStream().write(0);
										socket.getOutputStream().flush();
										socket.getOutputStream().close();
									}
								}
							}
							catch (Exception ex)
							{

							}
							finally
							{
								try
								{
									listener.close();
								}
								catch (IOException e)
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					});
					thrJoinThread.start();
					// /////////////////////////////////////////////////
					// ////// JOIN THREAD DONE
					// /////////////////////////////////////////////////

					// //////////////////////////////////////
					// // COMMUNICATION THREAD
					// //////////////////////////////////////
					thrCommunicateThread = new Thread(new Runnable()
					{
						@Override
						public void run()
						{
							while (true)
							{
								try
								{
									// System.out.println("Select an operation");
									// System.out.println("1. Send broadcast");
									// System.out.println("2. Remove a member from the group");
									// System.out.println("3. Exit");
									// System.out.print("Your choice:");
									// CURRENT_OPERATION = Integer
									// .parseInt(inputReader.readLine());

									if (CURRENT_OPERATION == 1)
									{
										CURRENT_OPERATION = 0;
										System.out.println("Enter message:");
										String message = txtMessage.getText();
										
										
										for (int count = 0; count < lstSockets
												.size(); count++)
										{
											try
											{
												Socket s = lstSockets
														.get(count);
												GKP gkp = lstGroupKeyPackets
														.get(count);

												OutputStreamWriter swriter = new OutputStreamWriter(
														s.getOutputStream());

												// Send the command for
												// receiving data
												swriter.write(1);

												// Send the key
												byte key[] = gkp
														.getCurrentKey()
														.getBytes();
												gkp.setCurrentKey(gkp
														.getNextKey());
												gkp.setNextKey(KeyGenerator
														.generateKey(16));
												System.out
														.println("Keys changed for member "
																+ (count + 1)
																+ " as " + gkp);
												lstGroupKeyPackets.set(count,
														gkp);

												for (int count2 = 0; count2 < key.length; count2++)
												{
													swriter.write(key[count2]);
													System.out
															.print(key[count2]
																	+ " ");
													Thread.sleep(1);
												}

												// Write Key Completion data
												swriter.write(0);
												swriter.flush();

												System.out
														.println("Key sent, now sending message...");
												// Encrypt and send message
												byte encrypted_message[] = CryptoClass.encrypt(
														gkp.getCurrentKey(),
														message.getBytes());// here we arre using crypoClass Encryption for encrypting the message 
												for (int count2 = 0; count2 < message
														.getBytes().length; count2++)
												{
													swriter.write(message
															.getBytes()[count2]);
													System.out
															.print(encrypted_message[count2]
																	+ " ");
													Thread.sleep(1);
												}

												// Send completion signal
												swriter.write(0);
												swriter.flush();
												System.out
														.println("Done for member "
																+ (count + 1));
											}
											catch (Exception ex)
											{
												System.out
														.println("Not sending broadcast for member "
																+ (count + 1));
											}
										}// end of for for Message transmission
										// We have already created
										// Socket object s so need
										// to go for this  Socket soc=new Socket(19999);
										
								//		OutputStream ou=lstGroupKeyPackets.getOutputStream();
																
											
									//		FileInputStream fis= new FileInputStream(txtFDetails.getText());
										//	int x=0;
										 //   while(true){
											//x=fis.read();
											//if(x==-1)break;
											//ou.write(x);
										   // }
										    //ou.close();
									}	
												
																												
									else if (CURRENT_OPERATION == 2)
									{
										CURRENT_OPERATION = 0;
										System.out
												.print("Enter member number(1-"
														+ lstSockets.size()
														+ "):");
										int NodeNumber = Integer
												.parseInt(inputReader
														.readLine());
										if (NodeNumber > lstSockets.size())
											continue;

										Socket s = lstSockets
												.get(NodeNumber - 1);
										BufferedWriter swriter = new BufferedWriter(
												new OutputStreamWriter(s
														.getOutputStream()));
										swriter.write(0);
										swriter.flush();
										swriter.close();

										System.out
												.println("Done closing connection "
														+ (NodeNumber));
									}
									else if (CURRENT_OPERATION == 3)
									{
										CURRENT_OPERATION = 0;
										// Close all streams
										for (int count = 0; count < lstSockets
												.size(); count++)
										{
											Socket s = lstSockets.get(count);
											BufferedWriter swriter = new BufferedWriter(
													new OutputStreamWriter(s
															.getOutputStream()));
											swriter.write(0);
											swriter.flush();
											swriter.close();

											System.out
													.println("Done closing connection "
															+ (count + 1));
										}
										// thrCommunicateThread.stop();
										// thrJoinThread.stop();
										// System.out.println("Threads Closed...");
										frame.dispose();
										System.exit(0);
										System.out.println("Exit");
										System.out.println("Exit done...");
										break;
									}
																		
																	
								}
								catch (Exception ex)
								{
									ex.printStackTrace();
									System.exit(0);
								}
							}
							System.out.println("Bye bye from Group Head!");
							frame.dispose();
							System.exit(1);
						}
					});
					// //////////////////////////////////////
					// // END OF COMMUNICATION THREAD
					// //////////////////////////////////////
				}
			});
			frame.add(btnStart);

			txtPortNumber = new JTextField();
			txtPortNumber.setText("19999");
			frame.add(txtPortNumber);

			txtMessage = new JTextField();
			txtMessage.addCaretListener(this);
			txtMessage.setText("Enter Message Here");
			frame.add(txtMessage);
			
			
			// FOr attaching File to send;	
			
						btnAttachFile=new JButton("Attach File");
						btnAttachFile.addActionListener(new ActionListener()
						{

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								JFileChooser attach=new JFileChooser();
								int retVal=attach.showOpenDialog(null);
								if (retVal==javax.swing.JFileChooser.APPROVE_OPTION)
								{
									java.io.File fil=attach.getSelectedFile();
									path = fil.toString();
									//txtFDetails.setText("\n"+txtFDetails.getText()+"\n"+path);
								}
								
								if (CURRENT_OPERATION==1)
								{
									CURRENT_OPERATION=1;
								}
							}
							
						});
						
						frame.add(btnAttachFile);
						
						txtFDetails = new JTextField();
						txtFDetails.addCaretListener(this);
						//txtFDetails.setText("Enter Message Here");
						frame.add(txtFDetails);
						
						
		// File Attachment operation ==1 				
			btnBroadcast = new JButton("Send broadcast");
			btnBroadcast.addActionListener(new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					
					//int i=0;
					for(Socket sck:lstfSockets)
					{
						//if(i%2 != 0)
						send(sck);System.out.println("send file...");
						//else
						//{CURRENT_OPERATION = 1;System.out.println("norm...");}
						//i++;					
					}
					CURRENT_OPERATION = 1;										
					System.out.println("count: "+lstfSockets.size());
				}
			});
			frame.add(btnBroadcast);
			
		
			btnRemoveMember = new JButton("Remove Member");
			btnRemoveMember.addActionListener(new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					CURRENT_OPERATION = 2;
				}
			});
			frame.add(btnRemoveMember);

			btnStop = new JButton("Exit");
			btnStop.addActionListener(new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					// TODO Auto-generated method stub
					CURRENT_OPERATION = 3;
				}
			});
			frame.add(btnStop);

			frame.setVisible(true);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}


public void send(Socket sck) //throws IOException 
		{		
	        try {
	   		 File file = new File(path);
			 Socket client = sck;
	         OutputStream outputStream = client.getOutputStream();

		    byte[] mybytearray = new byte[(int) file.length()]; //create a byte array to file
		    FileInputStream fileInputStream = new FileInputStream(file);
		    BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream); 		   
		    bufferedInputStream.read(mybytearray, 0, mybytearray.length); //read the file		    
		    outputStream.write(mybytearray, 0, mybytearray.length); //write file to the output stream byte by byte
		    outputStream.flush();
		    bufferedInputStream.close();
		    outputStream.close();
			System.out.println("\nFile "+txtFDetails.getText().trim()+" Sent...");    
		   } catch (UnknownHostException e) {
		   	System.out.println("UnknownHostException");
		    e.printStackTrace();
		   } catch (IOException e) {
		   	System.out.println("IOException");
				e.printStackTrace();
			} 
	      
	    }	
			


	@Override
	public void caretUpdate(CaretEvent arg0)
	{
		// GroupMember.test(txtMessage.getText();
	}
}
