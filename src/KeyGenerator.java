import java.util.Random;

public class KeyGenerator
{
	public static String generateKey(int KeyLength)
	{
		String key = "";
		Random rnd = new Random();

		while (key.length() != 16)
		{
			int val = rnd.nextInt(9);
			key = key + (val + "");
		}

		return key;
	}
}
