import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GroupMember
{
	public static String Message;
	public Thread thr;
	public Thread thrf;

	JFrame frame;
	JTextField txtIP;
	JTextField txtPort;
	JTextField txtKey;
	JTextArea txtOutput;
	JButton btnStart;
	boolean flag = false;

	public static void main(String[] args) throws IOException
	{
		new GroupMember();
	}

	public GroupMember()
	{
		try
		{
			frame = new JFrame("GKMP: Group Member");
			frame.setSize(400, 400);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLayout(new GridLayout(5, 1));
			// Here Grid layout will create 5 rows and 1 column

			txtIP = new JTextField("localhost");
			frame.add(txtIP);

			txtPort = new JTextField("19999");// We are using this port for all our communication
			frame.add(txtPort);

			txtKey = new JTextField("Enter group key");// Use for Joining
			frame.add(txtKey);

			txtOutput = new JTextArea("Output Appears Here");
			JScrollPane scrl = new JScrollPane(txtOutput);// means O/P can be bigger therefore scolling is provided
			frame.add(scrl);

			btnStart = new JButton("Start Member");
			btnStart.addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					thr = new Thread(new Runnable()
					{
						@Override
						public void run()
						{
							try
							{
								Socket s = new Socket(txtIP.getText(), Integer
										.parseInt(txtPort.getText()));
								BufferedWriter swriter = new BufferedWriter(
										new OutputStreamWriter(s
												.getOutputStream()));
								InputStreamReader sreader = new InputStreamReader(
										s.getInputStream());
								// System.out.println("Enter secret key:");
								int KEY = Integer.parseInt(txtKey.getText());// jo key provide karenge text box
								// me wo apan KEY naam ke variable me store karenge.
								System.out.println("Sending key:" + KEY);
								swriter.write(KEY);// Swriter is a object of BufferedWriter class
								// usko apan KEY pass karenge.
								swriter.flush();// Aur baad me sab clean karenge.

								while (true)
								{
									
									int Command = sreader.read();
									
									if (Command == 0)
										break;
									else if (Command == 1)
									{
										StringBuilder sbuild = new StringBuilder();
										sbuild.append("Reading message...\n");
										// InComming Message
										ArrayList<Byte> lstBytes = new ArrayList<Byte>();
										int byte_data;

										while ((byte_data = sreader.read()) != 0)
										{
											lstBytes.add((byte) byte_data);
										}

										sbuild.append("Key received\n");

										// Read encrypted message
										ArrayList<Byte> lstMessageBytes = new ArrayList<Byte>();
										while ((byte_data = sreader.read()) != 0)
										{
											lstMessageBytes
													.add((byte) byte_data);
										}
										sbuild.append("\nData received");
										byte key[] = new byte[lstBytes.size()];
										for (int count = 0; count < lstBytes
												.size(); count++)
											key[count] = lstBytes.get(count);

										byte message_[] = new byte[lstMessageBytes
												.size()];
										for (int count = 0; count < lstMessageBytes
												.size(); count++)
											message_[count] = lstMessageBytes
													.get(count);

										CryptoClass.decrypt(new String(key),
												message_);// here We have called the Decrypt method of cryptClass for decryption.

										String message = new String(message_);
										sbuild.append("\nDecrypted message:"
												+ message);
										flag = true;
										txtOutput.setText(sbuild.toString());
									}
								
								}
								System.out.println("Bye bye from Member!");
								System.exit(1);
							}
							catch (Exception ex)
							{

							}
						}
					});
					thr.start();
					
					
					
					
					
					
					
					
					
					
					
					thrf = new Thread(new Runnable()
					{
						@Override
						public void run()
						{
							try
							{
								
								
								System.out.println("rec file...");
								recfile();
							}
							catch (Exception ex)
							{

							}
						}
					});
					thrf.start();
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				}
			});
			frame.add(btnStart);

			frame.setVisible(true);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}



	public void recfile()
			{
				
			try{
			//while(true)
			//{
			Socket clientSocket = new Socket(txtIP.getText(), 19999);
			
			
			BufferedWriter swriter = new BufferedWriter(
										new OutputStreamWriter(clientSocket
												.getOutputStream()));
								
								// System.out.println("Enter secret key:");
								int KEY = Integer.parseInt(txtKey.getText())+1;// jo key provide karenge text box
								// me wo apan KEY naam ke variable me store karenge.
								System.out.println("Sending key:" + KEY);
								swriter.write(KEY);// Swriter is a object of BufferedWriter class
								// usko apan KEY pass karenge.
								swriter.flush();// Aur baad me sab clean karenge.

			
			
			InputStream inputStream;
    	 FileOutputStream fileOutputStream;
    	 BufferedOutputStream bufferedOutputStream;
    	 int filesize = 100000000; // filesize temporary hardcoded
    	 int bytesRead;
    	 int current = 0;	


	byte[] mybytearray = new byte[filesize];    //create byte array to buffer the file
 
        inputStream = clientSocket.getInputStream();
        fileOutputStream = new FileOutputStream("output");
        bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
 
        System.out.println("Receiving...");

        //following lines read the input slide file byte by byte
        bytesRead = inputStream.read(mybytearray, 0, mybytearray.length);
        current = bytesRead;
 
        do {
            bytesRead = inputStream.read(mybytearray, current, (mybytearray.length - current));
            if (bytesRead >= 0) {
                current += bytesRead;
            }
        } while (bytesRead > -1);
 
        bufferedOutputStream.write(mybytearray, 0, current);
        bufferedOutputStream.flush();
        bufferedOutputStream.close();
        inputStream.close();
        //clientSocket.wait();
        System.out.println("Server recieved the file");
	//recfile();
	//}
	}
	catch (IOException e){System.out.println(e.toString());}
	//catch	(InterruptedException e2){}
}



	public static void test(String data)
	{
		Message = data;
	}
}
